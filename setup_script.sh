#! /bin/bash

GIT_CONFIG_DIR=~/git/configs

function setup_desktop_environment() {
    THEME_NAME="Arc"
    FONT_NAME="DejaVu Sans 8"
    ICON_NAME="Faenza Dark"
    # install i3 and others
    sudo apt-get install -y \
         i3 \
         python-pip \
         gettext \
         feh \
         network-manager-gnome \
         pcmanfm \
         mc \
         ranger \
         solaar \
         volumeicon-alsa \
         faenza-icon-theme
    sudo pip install i3ipc udiskie
    mkdir -p $GIT_CONFIG_DIR
    pushd $GIT_CONFIG_DIR
    # setup i3
    git clone https://mmk86@bitbucket.org/mmk86/i3-config.git
    pushd i3-config
    mkdir ~/.i3
    cp * ~/.i3/
    mv ~/.i3/i3status.conf ~/.i3status.conf
    mkdir -p ~/Pictures
    ln -sf $(pwd)/wallpaper.png ~/Pictures/.wallpaper
    popd
    # setup theme
    sudo apt-get install -y \
         gnome-themes-standard \
         gtk2-engines-murrine \
         autoconf \
         automake \
         pkg-config \
         libgtk-3-dev
    git clone https://github.com/horst3180/arc-theme --depth 1
    pushd arc-theme
    ./autogen.sh --prefix=/usr
    sudo make install
    echo gtk-theme-name = "$THEME_NAME" > ~/.gtkrc-2.0
    echo gtk-font-name = "$FONT_NAME" >> ~/.gtkrc-2.0
    echo gtk-icon-theme-name = "$ICON_NAME" >> ~/.gtkrc-2.0
    echo '[Settings]' > ~/.config/gtk-3.0/settings.ini
    echo gtk-theme-name = $THEME_NAME >> ~/.config/gtk-3.0/settings.ini
    echo gtk-font-name = $FONT_NAME >> ~/.config/gtk-3.0/settings.ini
    echo gtk-icon-theme-name = $ICON_NAME >> ~/.config/gtk-3.0/settings.ini
    popd
    # end, restore directory
    popd
}

function setup_development_tools() {
    pushd $GIT_CONFIG_DIR
    sudo apt-get install -y \
         nodejs \
         nodejs-legacy \
         npm \
         openjdk-8-jdk \
         scala \
         emacs
    sudo npm install \
         tern \
         tern-jasmine \
         bower \
         karma-cli \
         mocha
    git clone https://mmk86@bitbucket.org/mmk86/emacs-config.git
    cp emacs-config/.emacs ~/
    popd
}

setup_desktop_environment
setup_development_tools
